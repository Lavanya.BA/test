import React, { useState } from "react";

import { TextBox } from '../../components/inputs';
import SearchCard from "../../components/searhCard";
import Spinner from "../../components/spinner";
import Pagination from "react-pagination-js";
import "react-pagination-js/dist/styles.css";

import axios from "axios";

import './style.css';

const Search = () => {
    const [formData, setFormData] = useState({});
    const [errormsg, seterrormsg] = useState({});
    const [state, setState] = useState({
        data: [],
        spinner: null,
        selectedData: [],
        currentPage: 0
    })

    const handleSearch = () => {
        let err = {}
        let isValid = true;
        if (!formData.search || formData.search.length <= 0) {
            err.search = "Please enter your keyword";
            isValid = false;
        }

        if (isValid === false) {
            seterrormsg(err);
            return;
        } else if (isValid === true) {
            seterrormsg({});
            setState((result) => {
                return {
                    ...result,
                    spinner: true
                }
            });
            getProductDetails(state?.currentPage);
        }
    }

    const getProductDetails = async (number) => {
        const config = [{
            'mode': 'no-cors',
            headers: {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*',
                'crossdomain': false
            }
        }];
        let url = `https://www.blibli.com/backend/search/products?searchTerm=${formData.search}&start=${number}&itemPerPage=24`;

        const getService = await axios.get(url, config);
        if (getService && Object.keys(getService.data).length > 0 && getService?.data?.code == 200) {
            setState((state) => {
                return { ...state, data: getService?.data?.data, spinner: false }
            }
            )
        }
        else if (getService && Object.keys(getService.data).length > 0 && getService?.data?.code != 200) {
            setState((state) => {
                return { ...state, spinner: false }
            });
            alert(getService?.data?.status)
        }
    }

    const handleDetails = (item) => {
        setState((result) => {
            return {
                ...result,
                selectedData: item
            }
        });
    }

    const handlePagination = (data) => {
        setState((result) => {
            return {
                ...result,
                currentPage: data,
                spinner: true
            }
        });
        getProductDetails(data);
        window.scrollTo(0, 0);
    }

    return (
        <div className='container text-center'>
            {
                state?.spinner === true &&
                <Spinner />
            }
            <>
                <div className="search">
                    <div className='row'>
                        <div className='col'>
                            <div className='row d-flex'>
                                <div className='col-sm-8'>
                                    <TextBox
                                        id="search"
                                        placeholder="search"
                                        formdata={formData}
                                        error={errormsg}
                                        maxLength={50}
                                        required
                                        autoComplete="off"
                                    />
                                </div>
                                <div className='col-sm-4'>
                                    <button
                                        className='btn btn-primary btn-block mt-0'
                                        onClick={() => {
                                            handleSearch()
                                        }}
                                    >Search</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className='m-4'>
                    <div className='row'>

                        {
                            state?.data &&
                                Object.keys(state?.data).length > 0 ?
                                (
                                    state?.data?.products.map((item) => {
                                        return (
                                            <div className='row p-5 searchBoby' key={item?.id}>
                                                <div
                                                    className="d-flex align-items-center"
                                                    onClick={() => handleDetails(item)}
                                                >
                                                    <SearchCard
                                                        data={item}
                                                    />
                                                </div>
                                            </div>
                                        )
                                    })
                                )
                                :
                                <span className="emptyResponse">No Results</span>
                        }
                    </div>
                </div>
                {state?.data && Object.keys(state?.data).length > 0 &&
                    Object.keys(state?.data?.paging).length > 0 &&
                    <Pagination
                        currentPage={state?.currentPage}
                        sizePerPage={state?.data?.paging?.item_per_page}
                        totalSize={state?.data?.paging?.total_item}
                        totalPages={state?.data?.paging?.total_page}
                        changeCurrentPage={(data) => handlePagination(data)}
                        theme={'border-bottom'}
                    />
                }
            </>
        </div>
    )
}

export default Search;
