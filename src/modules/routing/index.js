import React from 'react'
import {
    BrowserRouter,
    Routes,
    Route,
} from "react-router-dom";

import Login from '../login';
import Search from '../search';

const Routing = () => {
    return (
        <BrowserRouter>
            <Routes>
                <Route
                    exact
                    path="/"
                    element={
                        <Login />
                    }
                />
                <Route
                    exact
                    path="/home"
                    element={
                        <>
                            <header className="Header text-center">
                                <h3>Header Section</h3>
                            </header>
                            <main className='mt-5'>
                                <Search />
                            </main>
                            {/* <footer className="Footer text-center">
                                <h3>Footer Section</h3>
                            </footer> */}
                        </>
                    }
                />
            </Routes>
        </BrowserRouter>
    )
}

export default Routing;