import React, { useState } from "react";
import { Navigate } from "react-router-dom";
import { TextBox } from '../../components/inputs';

import BannerImg from './../../assets/home.webp';
import './style.css';

const Login = () => {
    const [formData, setFormData] = useState({});
    const [errormsg, seterrormsg] = useState({});
    const [isLoggedIn, setIsLoggedIn] = useState(false);

    const handleLogin = () => {
        let err = {}
        let isValid = true;
        if (!formData.username || formData.username.length <= 0) {
            err.username = "Please enter username";
            isValid = false;
        }
        if (!formData.password || formData.password.length <= 0) {
            err.password = "Please enter the password"
            isValid = false;
        }

        if (isValid === false) {
            seterrormsg(err);
            return;
        } else if (isValid === true) {
            seterrormsg({});
            let req = {
                username: formData?.username,
                password: formData?.password
            };
            if (req?.username === "testuser" && req?.password === 'Password@123') {
                setIsLoggedIn(true);
                setFormData({});
            } else {
                setIsLoggedIn(false);
                setFormData({});
            }
        }
    }

    return (
        <div className="login">
            {
                isLoggedIn === true && <Navigate to="/home" />
            }
            <div className="row h-100 m-0">
                <div className="col-sm p-0">
                    <img
                        src={BannerImg}
                        style={{ height: window.innerHeight, width: '100%' }}
                        alt={'banner image'}
                    />
                </div>
                <div className="col-sm">
                    <div className="row justify-content-center align-items-center h-100">
                        <div className="col-sm-8">
                            <div className="col-sm-12">
                                <TextBox
                                    label="Username"
                                    id="username"
                                    placeholder="Enter the username"
                                    formdata={formData}
                                    error={errormsg}
                                    maxLength={15}
                                    minLength={8}
                                    required
                                    autoComplete="off"
                                />
                            </div>
                            <div className="col-sm-12">
                                <TextBox
                                    label="Password"
                                    id="password"
                                    placeholder="Enter the password"
                                    formdata={formData}
                                    error={errormsg}
                                    maxLength={16}
                                    minLength={12}
                                    required
                                    autoComplete="off"
                                    isPassword
                                />
                            </div>
                            <div className='col-sm-12 mt-3'>
                                <button
                                    type="submit"
                                    className="btn btn-primary btn-block mt-0"
                                    onClick={() => handleLogin()}
                                >
                                    Login
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Login;
