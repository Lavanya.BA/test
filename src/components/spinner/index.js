import React from 'react';
import './style.css';

function Spinner() {
    return (
        <div className="loading">Loading&#8230;</div>
    )
}

export default Spinner
