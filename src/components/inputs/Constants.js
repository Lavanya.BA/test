﻿export const NameRegex = /^[ A-Za-z0-9'-]*$/;
export const UserNameRegex = /^[a-zA-Z0-9-_áàÁÀâÂ]*$/;
export const PasswordRegex = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[@@#?!$%^&*-]).{12,16}$/;