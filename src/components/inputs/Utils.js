﻿import * as Constants from './Constants';

const validateRegex = (data, rgx) => {
    let regex = new RegExp(rgx)
    data = data.replace(/\s/g, '');
    if (regex.test(data)) {
        return true;
    }
    else {
        return false;
    }
}

export {
    validateRegex
}