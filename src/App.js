import './App.css';
import Routing from "./modules/routing";

const App = () => {
  return (
    <div>
      <Routing />
    </div>
  );
}

export default App;
